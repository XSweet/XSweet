// This javascript file, run with Node.js, can be passed an HTML file
// ("htmlFile") output from XSweet, and return the same file with its
// images, referenced by path, encoded as base64 and embedded inline
// with the HTML.

const fs = require('fs');

// read XSweet HTML file as string
var htmlFile =
  fs.readFileSync(REFERENCE_TO_HTML_FILE)
  .toString();

// encode file to base64
const base64Encode = file => {
  return fs.readFileSync(file).toString('base64');
};

// create array of the img elements in the HTML file
const ogImgEls = htmlFile.match(/<img src="file:.*?">/g);
// create corresponding array of img paths
const imgPaths = ogImgEls.map(function(el) {
  return el.slice(15, el.lastIndexOf('"'));
});

// swap out img path elements with inline base64 picture elements
for(var i = 0; i < imgPaths.length; i++) {
  var ext = imgPaths[i].slice(imgPaths[i].lastIndexOf('.') + 1, imgPaths[i].length);
  var base64ImgEl = '<img src="data:image/' + ext + ';base64,' + base64Encode(imgPaths[i]) + '" />';
  htmlFile = htmlFile.replace(ogImgEls[i], base64ImgEl);
};

// overwrite original HTML file from XSweet
const output = fs.createWriteStream(REFERENCE_TO_HTML_FILE);
output.write(htmlFile);
