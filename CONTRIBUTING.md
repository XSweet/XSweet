# CONTRIBUTING

XSweet is both an open source software project (https://gitlab.coko.foundation/editoria/editoria) and an open community. We welcome people of all kinds to join the community and contribute with knowledge, skills, and expertise. Everyone is welcome in our chat room (https://mattermost.coko.foundation/coko/channels/xsweet).

In order to contribute to XSweet, you're expected to follow a few sensible guidelines.

## Search first, ask questions later

If you want to add a new XSLT step or change an existing step, or if you've experienced a bug or want to discuss something in the issue trackers, please search in the relevant repos to find out whether an issue already exists before you start developing. Issue lists for the main XSweet repos include:
* https://gitlab.coko.foundation/XSweet/XSweet/issues
* https://gitlab.coko.foundation/XSweet/HTMLevator/issues
* https://gitlab.coko.foundation/XSweet/editoria_typescript/issues

When in doubt about which repo your issue should be posted to, post it to XSweet.

## Discuss your contribution before you build

Please let us know about the contribution you plan to make before you start it. Either comment on a relevant existing issue, or open a new issue if you can't find an existing one. This helps us avoid duplicating effort and increases the likelihood that your contributions will be accepted. You can also ask in the chat room (https://mattermost.coko.foundation/coko/channels/xsweet) if you are unsure.

For contributions made as discussions and suggestions, feel free to open an RFC (requests for comment) as an issue at any time, so XSweet community members and maintainers can join a discussion.

## Branches

We maintain master as the production branch of the three main XSweet repos.

If you wish to contribute to XSweet, you should make a branch and then issue a pull request following this procedure:
1. Create a user account on Coko GitLab : http://gitlab.coko.foundation
2. Clone the desired repo's master branch with `git clone <XSweet repo URL>`
3. Create a new branch and work off that. Please name the branch descriptively so it identifies the feature you are working on. You can push the branch to Gitlab at any time.

## Getting your contributions merged

All merge requests should fulfill these two simple rules:
1.  Consensus should been established in its corresponding Gitlab issue
2.  Merge requests shouldn't break existing functionality

Once you're ready to submit your contributions:

* Generate a Merge Request (analogous to a GitHub Pull Request) from the GitLab interface but do not assign this request to anyone.
You do this from the Gitlab UI on your branch. As you submit your merge request, ask for feedback from both @wendell and @atheg:
  * @wendell will review the code and provide any comments, suggestions, etc. about implementation
  * @atheg will QC the code and provide feedback on a functional level, e.g. does it work?
  * We also encourage feedback and discussion from as many people as possible on merge requests!
* After discussion at this stage and/or altering your branch based on the discussion, you'll receive approval from both @wendell and @atheg. At this point, assign your merge request to @atheg to be merged. You do this from the Gitlab UI on your branch.

## Bug reports, feature requests, and support questions

Bugs should be reported as issues on the appropriate repo (when in doubt, add your issue to the XSweet/XSweet repo). Questions can be posted as issues, or posed in the XSweet Mattermost channel at https://mattermost.coko.foundation/coko/channels/xsweet
